package com.challenge.masivian.commons;

public class Constants {
    public static final Integer TWO = 2;
    public static final Integer NUMBER_OF_ROWS = 50;
    public static final Integer NUMBER_OF_COLUMNS = 4;
    public static final String MESSAGE_FOR_NOT_VALID_AMOUNT_OF_PRIME_NUMBERS= "Please Insert a value equal or more that 200";
    public static final String ROW_FORMAT = "%10d%10d%10d%10d";

}
