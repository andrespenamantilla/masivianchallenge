package com.challenge.masivian.bussiness;

import com.challenge.masivian.commons.Constants;
import com.challenge.masivian.ibussiness.ISplitPages;
import org.springframework.stereotype.Component;

import java.util.LinkedList;


/**
 * Represents a the class that has the methods for split the pages when an amount of prime numbers was founded
 *
 * @author Andrés Peña Mantilla
 * @version 1.0
 * @since 1.0
 */
@Component
public class SplitPages implements ISplitPages {

    private LinkedList<Integer> primeNumbersFounded;


    /** Print all the pages.
     * @param totalOfPrimerNumbers are the prime numbers founded to the splited in a sublist that contains a list of 'pages'
     */
    public LinkedList<LinkedList<Integer>> returnAmountOfPrimeNumbersForBePrinted(LinkedList<Integer> totalOfPrimerNumbers) {

        Integer totalOfPages;
        Integer numberOfElementsPerPage;
        Integer amountOfPrimeNumberFounded;

        primeNumbersFounded = totalOfPrimerNumbers;

        numberOfElementsPerPage = Constants.NUMBER_OF_ROWS * Constants.NUMBER_OF_COLUMNS;
        amountOfPrimeNumberFounded = totalOfPrimerNumbers.size();
        totalOfPages = (amountOfPrimeNumberFounded / numberOfElementsPerPage);

        LinkedList<LinkedList<Integer>> splitedPrimeNumberList = new LinkedList<>();

        Integer startIndex = 0;
        Integer finalIndex = 0;

        for (int i = 0; i < totalOfPages; i++) {

            if (i == 0) {
                startIndex = 0;
            }
            if (i >= 1) {
                startIndex = startIndex + numberOfElementsPerPage;

            }
            finalIndex = finalIndex + numberOfElementsPerPage;
            splitedPrimeNumberList.add(getSublistOfPrimeNumberPerInterval(startIndex, finalIndex));
        }
        return splitedPrimeNumberList;
    }

    /** This method get a list/page.
     * @param startIndex initial index to split a list
     * @param finalIndex final index to split a list
     */
    public LinkedList<Integer> getSublistOfPrimeNumberPerInterval(Integer startIndex, Integer finalIndex) {
        return new LinkedList<>(primeNumbersFounded.subList(startIndex, finalIndex));
    }

}
