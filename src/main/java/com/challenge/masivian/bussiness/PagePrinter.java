package com.challenge.masivian.bussiness;

import static com.challenge.masivian.commons.Constants.NUMBER_OF_ROWS;
import static com.challenge.masivian.commons.Constants.ROW_FORMAT;
import com.challenge.masivian.ibussiness.IPagePrinter;
import org.springframework.stereotype.Component;

import java.util.LinkedList;

/** Represents a the class that allows the method for print a page
 * @author Andrés Peña Mantilla
 * @version 1.0
 * @since 1.0
 */
@Component
public class PagePrinter implements IPagePrinter {

    /** Print all the pages.
     * @param totalOfPages is the list of list that contains the number of pages with the prime numbers for each page .
     */
    public String print(LinkedList<LinkedList<Integer>> totalOfPages) {
        String result = "";

        for (int pages = 0; pages < totalOfPages.size(); pages++) {
            System.out.println(" Prime Numbers === Page " + (pages + 1));
            System.out.println();

            int columnOne = 0;
            int columnTwo = NUMBER_OF_ROWS;
            int columnThree = NUMBER_OF_ROWS * 2;
            int columnFour = NUMBER_OF_ROWS * 3;

            for (int row = 0; row < NUMBER_OF_ROWS; row++) {

                String rowFormat = String.format(ROW_FORMAT, totalOfPages.get(pages).get(columnOne), totalOfPages.get(pages).get(columnTwo), totalOfPages.get(pages).get(columnThree), totalOfPages.get(pages).get(columnFour));
                System.out.println(rowFormat);
                result = result +rowFormat;
                ++columnOne;
                ++columnTwo;
                ++columnThree;
                ++columnFour;
            }
            System.out.println("\f");
        }
        return result;
    }
}
