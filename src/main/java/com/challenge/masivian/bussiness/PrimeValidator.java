package com.challenge.masivian.bussiness;

import static com.challenge.masivian.commons.Constants.TWO;
import static com.challenge.masivian.commons.Constants.MESSAGE_FOR_NOT_VALID_AMOUNT_OF_PRIME_NUMBERS;

import com.challenge.masivian.commons.Utils;
import com.challenge.masivian.ibussiness.IPrimeValidator;
import org.springframework.stereotype.Component;

import java.util.LinkedList;

/** Represents a the class that has the methods validate a primeNumber and find a list of them
 * @author Andrés Peña Mantilla
 * @version 1.0
 * @since 1.0
 */
@Component
public class PrimeValidator implements IPrimeValidator {

    /** Validate if a number is prime.
     * @param number number candidate.
     */
    public boolean isPrimeNumber(int number) {
        if (number == 1) {
            return false;
        }
        for (int divisor = 2; divisor <= number / 2; divisor++) {
            if (number % divisor == 0) {
                return false;
            }
        }
        return true;
    }

    /** Find a list of prime numbers.
     * @param amountOfTotalPrimeNumbers the total amount of prime numbers that are necessary to find .
     */
    public LinkedList<Integer> findAllPrimeNumbers(Integer amountOfTotalPrimeNumbers) {

        LinkedList<Integer> primeNumbers = new LinkedList<>();

        Integer candidateNumber;
        if (Utils.validateIfTotalOfPrimeNumbersAreEqualOrMoreThat200(amountOfTotalPrimeNumbers)) {
            System.out.println(MESSAGE_FOR_NOT_VALID_AMOUNT_OF_PRIME_NUMBERS);
            return new LinkedList<>();
        } else {
            primeNumbers.add(TWO);
            candidateNumber = TWO + 1;
            while (primeNumbers.size() < amountOfTotalPrimeNumbers) {
                if (isPrimeNumber(candidateNumber)) {
                    primeNumbers.add(candidateNumber);
                }
                candidateNumber = candidateNumber+1;
            }
        }
        return primeNumbers;
    }
}
