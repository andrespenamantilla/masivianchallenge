package com.challenge.masivian.controllers;

import com.challenge.masivian.ibussiness.IPagePrinter;
import com.challenge.masivian.ibussiness.IPrimeValidator;
import com.challenge.masivian.ibussiness.ISplitPages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PrimeNumbersController {

    IPrimeValidator primeValidator;
    ISplitPages splitPages;
    IPagePrinter pagePrinter;

    @Autowired
    public PrimeNumbersController(IPrimeValidator primeValidator, ISplitPages splitPages, IPagePrinter pagePrinter) {
        this.primeValidator = primeValidator;
        this.splitPages = splitPages;
        this.pagePrinter = pagePrinter;
    }

    @RequestMapping("/findAllPrimeNumbers")
    public String findAllPrimeNumbers(){
        return  pagePrinter.print(splitPages.returnAmountOfPrimeNumbersForBePrinted(primeValidator.findAllPrimeNumbers(1000)));

    }
}
