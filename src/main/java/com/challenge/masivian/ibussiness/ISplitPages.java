package com.challenge.masivian.ibussiness;

import java.util.LinkedList;

public interface ISplitPages {
    LinkedList<LinkedList<Integer>> returnAmountOfPrimeNumbersForBePrinted(LinkedList<Integer> totalOfPrimerNumbers);
    LinkedList<Integer> getSublistOfPrimeNumberPerInterval(Integer startIndex, Integer finalIndex);
}
