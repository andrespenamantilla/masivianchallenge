package com.challenge.masivian.ibussiness;

import java.util.LinkedList;

public interface IPrimeValidator {
    boolean isPrimeNumber(int number);
    LinkedList<Integer> findAllPrimeNumbers(Integer amountOfTotalPrimeNumbers);
}
