package com.challenge.masivian.ibussiness;

import java.util.LinkedList;

public interface IPagePrinter {

    String print(LinkedList<LinkedList<Integer>> totalOfPages);
}
