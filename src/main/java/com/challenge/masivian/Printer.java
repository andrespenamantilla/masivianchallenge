package com.challenge.masivian;

public class Printer {
    public static void main(String[] args) {
        //Cantidad de numeros a imprimir
        final int amountOfPrimeNUmbers = 1000;
        //Filas por pagina
        final int rows = 50;
        //Cantidad de Columnas
        final int columns = 4;
        final int ORDMAX = 30;
        int P[] = new int[amountOfPrimeNUmbers + 1];
        int PAGENUMBER;
        int PAGEOFFSET;
        int ROWOFFSET;
        int C;
        int J;
        int K;
        boolean JPRIME;
        int ORD;
        int SQUARE;
        int N = 0;
        int MULT[] = new int[ORDMAX + 1];
        J = 1;
        K = 1;
        P[1] = 2;
        ORD = 2;
        SQUARE = 9;
        while (K < amountOfPrimeNUmbers) {
            do {
                J += 2;
                if (J == SQUARE) {
                    ORD++;
                    SQUARE = P[ORD] * P[ORD];
                    MULT[ORD - 1] = J;
                }
                N = 2;
                JPRIME = true;
                while (N < ORD && JPRIME) {
                    while (MULT[N] < J)
                        MULT[N] += P[N] + P[N];
                    if (MULT[N] == J)
                        JPRIME = false;
                    N++;
                }
            } while (!JPRIME);
            K++;
            P[K] = J;
        }
        PAGENUMBER = 1;
        PAGEOFFSET = 1;
        while (PAGEOFFSET <= amountOfPrimeNUmbers) {
            System.out.print("The First ");
            System.out.print(Integer.toString(amountOfPrimeNUmbers));
            System.out.print(" Prime Numbers === Page ");
            System.out.print(Integer.toString(PAGENUMBER));
            System.out.println("\n");
            for (ROWOFFSET = PAGEOFFSET; ROWOFFSET <= PAGEOFFSET + rows - 1; ROWOFFSET++) {
                for (C = 0; C <= columns - 1; C++)
                    if (ROWOFFSET + C * rows <= amountOfPrimeNUmbers)
                        System.out.printf("%10d", P[ROWOFFSET + C * rows]);
                System.out.println();
            }
            System.out.println("\f");
            PAGENUMBER++;
            PAGEOFFSET += rows * columns;
        }
    }
}