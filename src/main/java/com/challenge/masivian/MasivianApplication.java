package com.challenge.masivian;

import com.challenge.masivian.ibussiness.IPagePrinter;
import com.challenge.masivian.ibussiness.IPrimeValidator;
import com.challenge.masivian.ibussiness.ISplitPages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;

import javax.annotation.PostConstruct;

/** Represents a Masivian Aplication Challenge.
 * @author Andrés Peña Mantilla
 * @version 1.0
 * @since 1.0
 */
@SpringBootApplication
public class MasivianApplication extends SpringBootServletInitializer {

    IPrimeValidator primeValidator;
    ISplitPages splitPages;
    IPagePrinter pagePrinter;

    @Autowired
    public MasivianApplication(IPrimeValidator primeValidator, ISplitPages splitPages, IPagePrinter pagePrinter) {
        this.primeValidator = primeValidator;
        this.splitPages = splitPages;
        this.pagePrinter = pagePrinter;
    }

    @PostConstruct
    public void init() {
        pagePrinter.print(splitPages.returnAmountOfPrimeNumbersForBePrinted(primeValidator.findAllPrimeNumbers(1000)));
    }


    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(MasivianApplication.class, args);
    }

}
