package com.challenge.masivian;

import com.challenge.masivian.commons.Constants;
import com.challenge.masivian.ibussiness.IPagePrinter;
import com.challenge.masivian.ibussiness.IPrimeValidator;
import com.challenge.masivian.ibussiness.ISplitPages;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.LinkedList;

@SpringBootTest
class MasivianApplicationTests {

    @Autowired
    private IPrimeValidator primeValidator;
    @Autowired
    private ISplitPages splitPages;
    @Autowired
    private IPagePrinter pagePrinter;

    @Test
    void validateThatProgramNotAllowsShowPrimeNumbersWhenValueIsLessThat200() {

        //Then
        LinkedList<Integer> resutlOfPrimeNumbers = primeValidator.findAllPrimeNumbers(100);
        String table = pagePrinter.print(splitPages.returnAmountOfPrimeNumbersForBePrinted(primeValidator.findAllPrimeNumbers(100)));

        //Assert
        Assert.assertTrue(resutlOfPrimeNumbers.isEmpty());
		Assert.assertTrue(table.isEmpty());
    }

    @Test
    void validateThatProgramNotAllowsShowPrimeNumbersWhenValueIsMoreOrEqualThat200() {

        //Then
        LinkedList<Integer> resutlOfPrimeNumbers = primeValidator.findAllPrimeNumbers(200);
		String table = pagePrinter.print(splitPages.returnAmountOfPrimeNumbersForBePrinted(primeValidator.findAllPrimeNumbers(200)));


		//Assert
        Assert.assertTrue(!resutlOfPrimeNumbers.isEmpty());
		Assert.assertTrue(!table.isEmpty());
    }


    @Test
    void validateThatTwoNumberIsTheOnlyOnePrimeNumber() {
        //The Scenario indicates that the only one number that is even number and prime is two

        //Assert
        Assert.assertTrue(primeValidator.isPrimeNumber(Constants.TWO));
    }

    @Test
    void validateThatAEvenNumberIsNotPrime() {
        //The Scenario indicates that the only one number that is even number and prime is two

        //Assert
        Assert.assertTrue(!primeValidator.isPrimeNumber(4));
		Assert.assertTrue(!primeValidator.isPrimeNumber(6));
		Assert.assertTrue(!primeValidator.isPrimeNumber(8));
		Assert.assertTrue(!primeValidator.isPrimeNumber(726));
    }

	@Test
	void validateThatOneIsNotPrime() {
		//The Scenario indicates that 1 is not prime number

		//Assert
		Assert.assertTrue(!primeValidator.isPrimeNumber(1));
	}

}
