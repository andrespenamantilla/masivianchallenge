# Encuentra los 1000 primeros primos

Para este caso la prueba consiste en realizar el refactoring de un código que calcula los mil (1000)
primeros numeros primos.

La clase base del refactor se puede ver en el paquete 'com.challenge.masivian'

IMPORT Y BUILD

Debido a que es un proyecto maven en cualquier IDE que tenga 
configurado un entorno de SPRING BOOT, es importar como proyecto maven esperar que
descargue las dependencias y esta listo. (hacer mvn clean, mvn build y mvn install si se hace manualmente)


EJECUCIÓN

Es sencillo despues de importar el proyecto ir a MasivianApplication.java y Correr la App.

CORRER LOS TEST

Ir a la clase MasivianApplicationTests.java y correr todas las pruebas unitarias para los
escenarios planteados.


PORQUE SPRING BOOT Y NO UNA APLICACIÓN STANDALONE

Porque a nivel de framework SpringBoot es sencillo de usar, facil de adaptar
y permite contextos de inyección de dependencias y exposición de endpoints 
de manera ágil y rápida ya que la construccíón con el springinizilr es muy
intuitiva
	
Si se desea obtener la informacion a traves de un endpoint la URL es 
http://localhost:8080/findAllPrimeNumbers y usar el metodo GET 
